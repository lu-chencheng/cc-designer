import CCard from "./CCard";
import MoveIcon from "./MoveIcon/index.vue";
import Divider from "./Divider";
import CTree from "./Tree";
import CFormItem from "./CFormItem";
export * from "./Message/index";
export * from "./Tabs/index";
export * from "./Collapse/index";

export { CCard, MoveIcon, Divider, CTree, CFormItem };
